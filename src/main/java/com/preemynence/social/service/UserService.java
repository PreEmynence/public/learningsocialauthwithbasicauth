package com.preemynence.social.service;
 
import java.util.Map;
import java.util.Optional;

import com.preemynence.social.dao.LocalUser;
import com.preemynence.social.dao.User;
import com.preemynence.social.dto.UserDTO;
import com.preemynence.social.exceptions.AuthorizationException;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;


public interface UserService {
 
    public User registerNewUser(UserDTO signUpRequest) throws AuthorizationException;
 
    User findUserByEmail(String email);
 
    Optional<User> findUserById(Long id);
 
    LocalUser processUserRegistration(String registrationId, Map<String, Object> attributes, OidcIdToken idToken, OidcUserInfo userInfo) throws AuthorizationException;
}