package com.preemynence.social.service.impl;

import com.preemynence.social.dao.LocalUser;
import com.preemynence.social.dao.Role;
import com.preemynence.social.dao.User;
import com.preemynence.social.dto.OAuth2UserInfo;
import com.preemynence.social.dto.OAuth2UserInfoFactory;
import com.preemynence.social.dto.SocialProvider;
import com.preemynence.social.dto.UserDTO;
import com.preemynence.social.exceptions.AuthorizationException;
import com.preemynence.social.exceptions.OAuth2AuthenticationProcessingException;
import com.preemynence.social.repo.RoleRepository;
import com.preemynence.social.repo.UserRepository;
import com.preemynence.social.service.UserService;
import com.preemynence.social.utils.GeneralUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.core.oidc.OidcIdToken;
import org.springframework.security.oauth2.core.oidc.OidcUserInfo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Override
	@Transactional(value = "transactionManager")
	public User registerNewUser(final UserDTO signUpRequest) throws AuthorizationException {
		if (userRepository.existsByEmail(signUpRequest.getEmail())) {
			throw new AuthorizationException("User with email id " + signUpRequest.getEmail() + " already exist");
		}
		User user = buildUser(signUpRequest);
		user.setCreatedBy(user.getFullName());
		user.setCreatedTs(Timestamp.from(Instant.now()));
		user.setUpdatedBy(user.getFullName());
		user.setUpdatedTs(Timestamp.from(Instant.now()));
		user = userRepository.save(user);
		userRepository.flush();
		return user;
	}

	private User buildUser(final UserDTO userDTO) {
		User user = new User();
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		user.setEmail(userDTO.getEmail());
		user.setPassword(new BCryptPasswordEncoder().encode(userDTO.getPassword()));
		final HashSet<Role> roles = new HashSet<>();
		roles.add(roleRepository.findByName(Role.ROLE_ADMIN));
		user.setRoles(roles);
		user.setExProvider(userDTO.getSocialProvider().getProviderType());
		user.setExProviderUserId(userDTO.getSocialProviderId());
		user.setEnabled(true);
		return user;
	}

	@Override
	public User findUserByEmail(final String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	@Transactional
	public LocalUser processUserRegistration(String registrationId, Map<String, Object> attributes, OidcIdToken idToken, OidcUserInfo userInfo) throws AuthorizationException {
		OAuth2UserInfo oAuth2UserInfo = OAuth2UserInfoFactory.getOAuth2UserInfo(registrationId, attributes);
		if (StringUtils.isEmpty(oAuth2UserInfo.getName())) {
			throw new OAuth2AuthenticationProcessingException("Name not found from OAuth2 provider");
		} else if (StringUtils.isEmpty(oAuth2UserInfo.getEmail())) {
			throw new OAuth2AuthenticationProcessingException("Email not found from OAuth2 provider");
		}
		UserDTO userDetails = toUserRegistrationObject(registrationId, oAuth2UserInfo);
		User user = findUserByEmail(oAuth2UserInfo.getEmail());
		if (user != null) {
			if (!user.getExProvider().equals(registrationId) && !user.getExProvider().equals(SocialProvider.LOCAL.getProviderType())) {
				throw new OAuth2AuthenticationProcessingException(
						"Looks like you're signed up with " + user.getExProvider() + " account. Please use your " + user.getExProvider() + " account to login.");
			}
			user = updateExistingUser(user, oAuth2UserInfo);
		} else {
			user = registerNewUser(userDetails);
		}

		return LocalUser.create(user, attributes, idToken, userInfo);
	}

	private User updateExistingUser(User existingUser, OAuth2UserInfo oAuth2UserInfo) {
		existingUser.setFirstName(oAuth2UserInfo.getName());
		return userRepository.save(existingUser);
	}

	private UserDTO toUserRegistrationObject(String registrationId, OAuth2UserInfo oAuth2UserInfo) {
		UserDTO userDTO = new UserDTO();
		userDTO.setSocialProviderId(oAuth2UserInfo.getId());
		userDTO.setFirstName(oAuth2UserInfo.getName());
		userDTO.setEmail(oAuth2UserInfo.getEmail());
		userDTO.setSocialProvider(GeneralUtils.toSocialProvider(registrationId));
		userDTO.setPassword("changeit");
		return userDTO;
	}

	@Override
	public Optional<User> findUserById(Long id) {
		return userRepository.findById(id);
	}
}