package com.preemynence.social.dto;

import com.preemynence.social.dao.Role;
import lombok.Data;

import java.util.Set;

@Data
public class UserDTO {
	private String email;
	private String firstName;
	private String lastName;
	private String password;
	private Set<Role> roles;
	private boolean enabled;
	private SocialProvider socialProvider;
	private String socialProviderId;

}
