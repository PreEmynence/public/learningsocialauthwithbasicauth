package com.preemynence.social.dao;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.sql.Timestamp;

@Data
@MappedSuperclass
public class BaseEntity implements Serializable {

	@Column(name = "created_by", length = 50)
	protected String createdBy;

	@Column(name = "updated_by", length = 50)
	protected String updatedBy;

	@Column(name = "created_ts", nullable = false)
	protected Timestamp createdTs;

	@Column(name = "updated_ts", nullable = false)
	protected Timestamp updatedTs;

	@Override
	public String toString() {
		return "BaseEntity{" +
				"createdBy='" + createdBy + '\'' +
				", updatedBy='" + updatedBy + '\'' +
				", createdTs=" + createdTs +
				", updatedTs=" + updatedTs +
				'}';
	}
}
