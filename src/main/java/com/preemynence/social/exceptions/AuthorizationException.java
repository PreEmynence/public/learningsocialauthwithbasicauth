package com.preemynence.social.exceptions;

import org.springframework.http.HttpStatus;

public class AuthorizationException extends Exception {

	private HttpStatus httpStatus = HttpStatus.CONFLICT;

	public AuthorizationException(String errorMessage) {
		super(errorMessage);
	}

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}
}
