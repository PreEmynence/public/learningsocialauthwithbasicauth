package com.preemynence.social.conroller;

import com.nimbusds.oauth2.sdk.pkce.CodeVerifier;
import com.preemynence.social.config.TokenProvider;
import com.preemynence.social.dao.LocalUser;
import com.preemynence.social.dao.User;
import com.preemynence.social.dto.ApiResponse;
import com.preemynence.social.dto.JwtAuthenticationResponse;
import com.preemynence.social.dto.LoginRequest;
import com.preemynence.social.dto.UserDTO;
import com.preemynence.social.exceptions.AuthorizationException;
import com.preemynence.social.service.UserService;
import com.preemynence.social.utils.GeneralUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping("/api/auth")
public class AuthController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserService userService;

	@Autowired
	TokenProvider tokenProvider;

	@PostMapping("/signin")
	public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
		Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequest.getEmail(), loginRequest.getPassword()));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		LocalUser localUser = (LocalUser) authentication.getPrincipal();
		String jwt = tokenProvider.createToken(localUser, true);
		return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, true, GeneralUtils.buildUserInfo(localUser)));
	}

	@PostMapping("/signup")
	public ResponseEntity<?> registerUser(@Valid @RequestBody UserDTO signUpRequest) {
		try {
			User user = userService.registerNewUser(signUpRequest);
		} catch (AuthorizationException e) {
			log.error("Exception Ocurred", e);
			return new ResponseEntity<>(new AuthorizationException("Email Address already in use!"), HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok().body(new ApiResponse(true, "User registered successfully"));
	}
}